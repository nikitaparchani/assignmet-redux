import React, { useState } from "react";
import { connect } from "react-redux";
import "./../App.css";
import { fetchMovies } from "./../store/actions/movieAction";

const Movie = React.memo(({ movies, fetchMovies }) => {
  const [page, setPage] = useState(movies.page);
  const [detailsOpen, setDetailsOpen] = useState(false);
  const loadMore = () => {
    movies.page = movies.page + 1;
    fetchMovies({ search: movies.search, page: movies.page });
  };

  const onDetailsOpen = () => {
    setDetailsOpen(true);
  };

  return (
    <div className="container">
      {movies.movies.length > 0 ? (
        <p className="requestUrl">
          <span className="requestData">Requesting:</span>{" "}
          https://api.jikan.moe/v3/search/anime?q=
          {movies.search}
          &limit=16&page={movies.page}
        </p>
      ) : (
        <p></p>
      )}

      <div className="row searchData pd-l-44">
        {movies.movies.length > 0 &&
          movies.movies.map((image, index) => {
            const { title, image_url } = image;
            return (
              <div
                key={index}
                style={{
                  width: "18rem",
                  borderRadius: "10px",
                }}
                className="card"
              >
                <img
                  alt={title}
                  data-src={image_url}
                  style={{ height: "18rem" }}
                  className="card-img-top"
                  src={image_url}
                  onClick={() => onDetailsOpen(image)}
                />

                <div className="card-body">
                  <h5
                    className="card-title"
                    style={{
                      textOverflow: "",
                      padding: "auto",
                    }}
                  >
                    {title}
                  </h5>
                </div>
              </div>
            );
          })}
      </div>
      {movies.fetching ? (
        <h3>Loading..Please Wait...</h3>
      ) : movies.movies.length > 0 ? (
        <button className="loadMoreBtn" onClick={() => loadMore()}>
          Load More
        </button>
      ) : (
        <h3 className="searchData">No Data</h3>
      )}
    </div>
  );
});

const mapsStateToProps = ({ movies }) => {
  return { movies: movies };
};

export default connect(mapsStateToProps, { fetchMovies })(Movie);
