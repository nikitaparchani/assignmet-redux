import React from "react";
import Nav from "./Nav";
import Movie from "./Movie";

function App() {
  return (
    <div className="App">
      <Nav />
      <Movie />
    </div>
  );
}

export default App;
