import axios from "axios";
import { CloseButton } from "react-bootstrap";

// make API calls and pass the returned data via dispatch
export const fetchMovies = (data) => async (dispatch) => {
  const { search } = data;
  dispatch({ type: "FETCHING_MOVIES", fetching: true });
  await axios(
    `https://api.jikan.moe/v3/search/anime?q=${data.search}&limit=16&page=${data.page}`
  )
    .then((response) => {
      const results = response.data.results;
      dispatch({ type: "STACK_MOVIES", results });
      dispatch({ type: "MOVIE_SEARCHED", search });
      dispatch({ type: "FETCHING_MOVIES", fetching: false, page: data.page });
    })
    .catch((e) => {
      const message = e && e.response && e.response.data;
      console.log(message);
      if (message.status === 404) {
        dispatch({ type: "FETCHING_MOVIES", fetching: false });
        alert("No More data");
      }
      return e;
    });
};

export const resetMovies = () => async (dispatch) => {
  dispatch({ type: "RESET_MOVIES", payload: [], page: 1 });
};
